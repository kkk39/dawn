#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>
#include<unistd.h>

//全局变量
int high,width;                               //游戏画面大小
int ball_x,ball_y;                            //小球的坐标
int ball_vx,ball_vy;                          //小球的速度
int position_x,position_y;                    //挡板的中心坐标
int ridus;                                    //挡板的半径大小 
int left,right;                               //挡板的左右位置

void gotoxy(int x,int y)                      //将光标移动到（x,y)位置 
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle,pos);
 } 
 
 void startup()                                //数据的初始化
 {
 	high = 15;
 	width = 20;
 	ball_x = 0;
 	ball_y = width/2;
 	ball_vx = 1;
 	ball_vy = 1;
 	ridus = 5;
 	position_x = high;
 	position_y = width/2;
 	left = position_y - ridus;
 	right = position_x + ridus;
  } 
  
  void show()                                  //显示画面
{
	gotoxy(0,0);
	int i,j;
	for(i = 0;i<high+1;i++)
	{
		for(j = 0;j<=width;j++)
		{
			if((i == ball_x)&&(j== ball_y))
			       printf("0");                    //输出小球 
			else if(j == width)
			       printf("|");               //输出有边框 
			else if(i== high)
			        printf("-");               //输出下边框 
			else if((i==high-1)&&(j>=left)&&(j<=right))
			        printf("*");              //输出挡板 
			else
			    printf(" ");                   //输出空格 
		}
		printf("\n"); 
	}
 } 
 
 void updateWithoutInput()                     //与用户输入无关的更新
 {
 	ball_x = ball_x+ball_vx;
 	ball_y = ball_y+ball_vy;
 	if((ball_x==0)||(ball_x==high-1))
 	   ball_vx = -ball_vx;
 	if((ball_y==0)||(ball_y==width-1))
 	   ball_vy = -ball_vy;
 	usleep(50000);
  } 
  
  void updateWithInput()                       //与用户输入有关的更新
  {
  	char input;
  	if(kbhit())
  	{
  		input = getch();                      //判断是否有输入
		if(input == 'a')
		{
			position_y--;                     //位置左移 
			left = position_y - ridus;
			right = position_y + ridus;
			
		 } 
		if(input == 'd')
		{
			position_y++;                      //位置右移 
			left = position_y - ridus; 
			right = position_y + ridus;
		}
	  }
   } 
   
int main() 
   {
    startup();                                //数据的初始化
   	while(1)
   	{
   		show();                                    //显示画面
		updateWithoutInput();                     //与用户输入无关的更新
		updateWithInput();                       //与用户输入有关的更新
	   }
	   return 0;
   }
