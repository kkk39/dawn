#include<graphics.h>
#include<conio.h>
#include<math.h>

//����Windows Multimendia APT
#pragma comment(lib,"Winmm.lib")

#define Width 400
#define High 500

IMAGE img_bk;    //����ͼƬ
int position_x, position_y;        //�ɻ���λ��
int bullet_x, bullet_y;            //�ӵ���λ��
float enemy_x, enemy_y;            //�л���λ��
IMAGE img_planeNormal1, img_planeNormal2;    //�ɻ�ͼƬ
IMAGE img_planeExplode1, img_planeExplode2; //��ը�ɻ�ͼƬ
IMAGE img_bullet1, img_bullet2;             //�ӵ���ͼƬ
IMAGE img_enemyplane1, img_enemyplane2;     //�л���ͼƬ
int isExpolde = 0;							//�ɻ��Ƿ�ը

void startup()
{
	initgraph(Width, High);
	loadimage(&img_bk, "D:\\c����\\��ϷͼƬ�ز�\\background.jpg");
	loadimage(&img_planeNormal1, "D:\\c����\\��ϷͼƬ�ز�\\planeNormal2.jpg");
	loadimage(&img_planeNormal2, "D:\\c����\\��ϷͼƬ�ز�\\planeNormal1.jpg");
	loadimage(&img_bullet1, "D:\\c����\\��ϷͼƬ�ز�\\bullet2.jpg");
	loadimage(&img_bullet2, "D:\\c����\\��ϷͼƬ�ز�\\bullet1.jpg");
	loadimage(&img_enemyplane1, "D:\\c����\\��ϷͼƬ�ز�\\enemyplane2.jpg");
	loadimage(&img_enemyplane2, "D:\\c����\\��ϷͼƬ�ز�\\enemyplane1.jpg");
	position_x = High * 0.7;
	position_y = Width * 0.5;
	bullet_x = position_x;
	bullet_y = -85;
	enemy_x = Width * 0.5;
	enemy_y = 10;
	BeginBatchDraw();
}

void show()
{
	putimage(0, 0, &img_bk);                 //��ʾ����
	if (isExpolde == 0)
	{
		putimage(position_x - 50, position_y - 60, &img_planeNormal1, NOTSRCERASE);  //��ʾ�ɻ�
		putimage(position_x - 50, position_y - 60, &img_planeNormal2, SRCINVERT);
		putimage(bullet_x - 7, bullet_y, &img_bullet1, NOTSRCERASE);
		putimage(bullet_x - 7, bullet_y, &img_bullet2, SRCINVERT);
		putimage(enemy_x, enemy_y, &img_enemyplane1, NOTSRCERASE);                   //��ʾ�л�
		putimage(enemy_x, enemy_y, &img_enemyplane2, SRCINVERT);
	}
	else
	{
		putimage(position_x - 50, position_y - 60, &img_planeExplode2, NOTSRCERASE);    //��ʾ��ը�ɻ�
		putimage(position_x - 50, position_y - 60, &img_planeExplode1, SRCINVERT);
	}
	FlushBatchDraw();
	Sleep(2);
}

void updateWithoutIput()
{
	if (bullet_y > -25)
		bullet_y = bullet_y - 3;
	if (enemy_y < High - 25)
		enemy_y = enemy_y + 0.5;
	else
		enemy_y = 10;
	if (fabs(bullet_x - enemy_x) + fabs(bullet_y - enemy_y) < 50)                   //�ӵ����ел�
	{
		enemy_x = rand() % Width;
		enemy_y = -40;
		bullet_y = -85;
	}
	if (fabs(position_x - enemy_x) + fabs(position_y - enemy_y) < 150)             //�л�ײ���һ�
		isExpolde = 1;
}

void updateWithInput()
{
	MOUSEMSG m;                          //���������Ϣ
	while (MouseHit())                   //����Ƿ��������Ϣ
	{
		m = GetMouseMsg();
		if (m.uMsg == WM_MOUSEMOVE)
		{
			//�ɻ���λ�õ���������ڵ�λ��
			position_x = m.x;
			position_y = m.y;
		}
		else if (m.uMsg == WM_LBUTTONDOWN)
		{	//���������������ӵ�
			bullet_x = position_x;
			bullet_y = position_y - 85;
		}
	}
}

void gameover()
{
	EndBatchDraw();
	_getch();
	closegraph();
}

int main()
{
	startup();
	while (1)
	{
		show();
		updateWithoutIput();
		updateWithInput();
	}
	gameover();
	return 0;
}