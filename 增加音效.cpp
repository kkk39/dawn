#include<graphics.h>
#include<conio.h>
#include<math.h>

//引用Windows Multimendia APT
#pragma comment(lib,"Winmm.lib")

#define Width 400
#define High 500

IMAGE img_bk;    //背景图片
float position_x, position_y;        //飞机的位置
float bullet_x, bullet_y;            //子弹的位置
float enemy_x, enemy_y;            //敌机的位置
IMAGE img_planeNormal1, img_planeNormal2;   //飞机图片
IMAGE img_planeExplode1, img_planeExplode2; //爆炸飞机图片
IMAGE img_bullet1, img_bullet2;             //子弹的图片
IMAGE img_enemyplane1, img_enemyplane2;     //敌机的图片
int isExpolde = 0;							//飞机是否爆炸
int score = 0;                              //得分

void startup()
{
	mciSendString("open ", NULL, 0, NULL);      //打开背景音乐
	mciSendString("play bkmusic repeat", NULL, 0, NULL);   //循环播放
	initgraph(Width, High);
	loadimage(&img_bk, "D:\\c语言\\游戏图片素材\\background.jpg");
	loadimage(&img_planeNormal1, "D:\\c语言\\游戏图片素材\\planeNormal2.jpg");
	loadimage(&img_planeNormal2, "D:\\c语言\\游戏图片素材\\planeNormal1.jpg");
	loadimage(&img_bullet1, "D:\\c语言\\游戏图片素材\\bullet2.jpg");
	loadimage(&img_bullet2, "D:\\c语言\\游戏图片素材\\bullet1.jpg");
	loadimage(&img_enemyplane1, "D:\\c语言\\游戏图片素材\\enemyplane2.jpg");
	loadimage(&img_enemyplane2, "D:\\c语言\\游戏图片素材\\enemyplane1.jpg");
	position_x = Width * 0.5;
	position_y = High * 0.7;
	bullet_x = position_x;
	bullet_y = -85;
	enemy_x = Width * 0.5;
	enemy_y = 10;
	BeginBatchDraw();
}

void show()
{
	putimage(0, 0, &img_bk);                 //显示背景
	if (isExpolde == 0)
	{
		putimage(position_x - 50, position_y - 60, &img_planeNormal1, NOTSRCERASE);  //显示飞机
		putimage(position_x - 50, position_y - 60, &img_planeNormal2, SRCINVERT);
		putimage(bullet_x - 7, bullet_y, &img_bullet1, NOTSRCERASE);                 //显示子弹
		putimage(bullet_x - 7, bullet_y, &img_bullet2, SRCINVERT);
		putimage(enemy_x, enemy_y, &img_enemyplane1, NOTSRCERASE);                   //显示敌机
		putimage(enemy_x, enemy_y, &img_enemyplane2, SRCINVERT);
	}
	else
	{
		putimage(position_x - 50, position_y - 60, &img_planeExplode1, NOTSRCERASE);    //显示爆炸飞机
		putimage(position_x - 50, position_y - 60, &img_planeExplode2, SRCINVERT);
	}
	outtextxy(Width * 0.48, High * 0.95, "得分： ");
	char s[5];
	printf(s, "%d", score);
	outtextxy(Width * 0.55, High * 0.95, s);
	FlushBatchDraw();
	Sleep(2);
}

void updateWithoutIput()
{
	if (isExpolde == 0)
	{
		if (bullet_y > -25)
			bullet_y = bullet_y - 3;
		if (enemy_y < High - 25)
			enemy_y = enemy_y + 0.5;
		else
			enemy_y = 10;
	}
	if (fabs(bullet_x - enemy_x) + fabs(bullet_y - enemy_y) < 80)                   //子弹击中敌机
	{
		enemy_x = rand() % Width;
		enemy_y = -40;
		bullet_y = -85;
		mciSendString("close gemusic", NULL, 0, NULL);                       //先把前面一次音乐关闭
		mciSendString("open ", NULL, 0, NULL);                               //打开音乐
		mciSendString("play gemusic", NULL, 0, NULL);                        //仅播放一次
		score++;
		if (score > 0 && score % 5 == 0 && score % 2 != 0)
		{
			mciSendString("close 5music", NULL, 0, NULL);                       //先把前面一次音乐关闭
			mciSendString("open ", NULL, 0, NULL);                               //打开音乐
			mciSendString("play 5music", NULL, 0, NULL);                        //仅播放一次
		}
		if (score % 10 == 0)
		{
			mciSendString("close 10music", NULL, 0, NULL);                       //先把前面一次音乐关闭
			mciSendString("open ", NULL, 0, NULL);                               //打开音乐
			mciSendString("play 10music", NULL, 0, NULL);                        //仅播放一次
		}
	}
	if (fabs(position_x - enemy_x) + fabs(position_y - enemy_y) < 150)             //敌机撞中我机
	{
		isExpolde = 1;
		mciSendString("close exmusic", NULL, 0, NULL);                       //先把前面一次音乐关闭
		mciSendString("open ", NULL, 0, NULL);                               //打开音乐
		mciSendString("play exmusic", NULL, 0, NULL);                        //仅播放一次
	}
}


void updateWithInput()
{
	if (isExpolde == 0)
	{
		MOUSEMSG m;                          //定义鼠标消息
		while (MouseHit())                   //检测是否有鼠标消息
		{
			m = GetMouseMsg();
			if (m.uMsg == WM_MOUSEMOVE)
			{
				//飞机的位置等于鼠标所在的位置
				position_x = m.x;
				position_y = m.y;
			}
			else if (m.uMsg == WM_LBUTTONDOWN)
			{	//按下鼠标左键发射子弹
				bullet_x = position_x;
				bullet_y = position_y - 85;
				mciSendString("close fgmusic", NULL, 0, NULL);                       //先把前面一次音乐关闭
				mciSendString("open ", NULL, 0, NULL);                               //打开音乐
				mciSendString("play fgmusic", NULL, 0, NULL);
			}
		}
	}
}

void gameover()
{
	EndBatchDraw();
	_getch();
	closegraph();
}

int main()
{
	startup();
	while (1)
	{
		show();
		updateWithoutIput();
		updateWithInput();
	}
	gameover();
	return 0;
}